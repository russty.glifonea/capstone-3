import {Button, Row, Col} from 'react-bootstrap'

export default function Banner(){
	return(
		<Row>
			<Col className="p-5">
				<h1>One Stop, Baby Shop</h1>
				<p>Shopping for babies garments is not not an easy task, but if your business provides a helping to the parents, then it could be.</p>
				<Button variant="primary">Shop now!</Button>
			</Col>
		</Row>
	)
}