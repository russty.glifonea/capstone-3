import { Row, Col, Card } from 'react-bootstrap'

export default function Highlights(){
	return(
		<Row>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3">
		            <Card.Body>
		                <Card.Title>
		                    <h2>Find Your perfect Baby Carrier</h2>
		                </Card.Title>
		                <Card.Text>
		                    Babywearing is an amazing parenting tool but it can be overwhelming as a first time parent or caregiver. Our aim is to make your babywearing journey easy and enjoyable!.
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3">
		            <Card.Body>
		                <Card.Title>
		                    <h2>Made with Love for Our Little Ones</h2>
		                </Card.Title>
		                <Card.Text>
		                    We, at Baby On Board believe in good quality, eco-friendly & natural clothes for babies. We are a family business that make products with soul.
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3">
		            <Card.Body>
		                <Card.Title>
		                    <h2>Lowest Prices Guaranteed</h2>
		                </Card.Title>
		                <Card.Text>
		                   Chescaleys Baby Shop price match applies only to Philippines interenet retailer that have the identical products and /or goods IN STOCK available.
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		</Row>
	)
}