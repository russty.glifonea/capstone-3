import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function ProductView() {

	// Gets the productId from the URL of the route that this component is connected to. '/products/:productId'
	const {productId} = useParams()

	const {user} = useContext(UserContext)

	const navigate = useNavigate()

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [color, setColor] = useState(0);
	const [size, setSize] = useState(0);
	const [quantity, setQuantity] = useState(0);

	const order = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/order`, {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				userId: user.id
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result) {
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "You have enrolled successfully!"
				})

				navigate('/product')
			} else {
				console.log(result)

				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again :("
				})
			}
		})
		// We may put the error alert in a catch block
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`)
		.then(response => response.json())
		.then(result => {
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
			setColor(result.color)
			setSize(result.size)
			setQuantity(result.quantity)
		})
	}, [productId])

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Card.Subtitle>Color</Card.Subtitle>
							<Card.Text>{color}</Card.Text>
							<Card.Subtitle>Size</Card.Subtitle>
							<Card.Text>{size}</Card.Text>
							<Card.Subtitle>Quantity</Card.Subtitle>
							<Card.Text>{quantity}</Card.Text>
							{	user.id !== null ? 
									<Button variant="primary" onClick={() => order(productId)}>Order</Button>
								:
									<Link className="btn btn-danger btn-block" to="/login">Log in to Order</Link>
							}
							
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}