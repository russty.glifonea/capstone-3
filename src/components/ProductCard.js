import {useState, useEffect} from 'react'
import {Card, Button} from 'react-bootstrap'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'

export default function ProductCard({product}){

	// Destructuring the props
	const {name, description, price, color, size, quantity, _id} = product

	// Initialize a 'count' state with a value of zero (0)
	const [count, setCount] = useState(0) 
	const [slots, setSlots] = useState(1000)
	const [isOpen, setIsOpen] = useState(true)

	// function enroll(){
	// 	if(slots > 0){
	// 		setCount(count + 1)
	// 		setSlots(slots - 1)

	// 		return 
	// 	}

	// 	alert('Slots are full!')
	// }

	// Effects in React is just like side effects/effects in real life, where everytime something happens within the component, a function or condition runs. 
	// You may also listen or watch a specific state for changes instead of watching/listening to the whole component
	// useEffect(() => {
	// 	if(slots === 0){
	// 		setIsOpen(false)
	// 	}
	// }, [slots])

	return(
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PHP {price}</Card.Text>
				<Card.Subtitle>Color:</Card.Subtitle>
				<Card.Text>{color}</Card.Text>
				<Card.Subtitle>Size:</Card.Subtitle>
				<Card.Text>{size}</Card.Text>
				<Card.Subtitle>Quantity:</Card.Subtitle>
				<Card.Text>{quantity}</Card.Text>
				<Link className="btn btn-primary" to={`/product/${_id}`}>Details</Link>
			</Card.Body>
		</Card>
	)
}

// Prop Types can be used to validate the data coming from the props. You can define each property of the prop and assign specific validation for each of them.
ProductCard.propTypes = {
	product: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		color: PropTypes.string.isRequired,
		size: PropTypes.string.isRequired,
		quantity: PropTypes.number.isRequired
	})
}