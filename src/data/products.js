const product_data = [
	{
		id: "wdc001",
		name: "RomperinBox Baby Football Jersey Onsies Raglan Bodysuit Customized Newborn 2022 World Cup Sport Teams Unisex Clothes Romper",
		description: "Football baby jersey bodysuit short sleeve customized 2022 World-Cup sport onsies romper sublimation clothes family photo-Navy 1-pack 3-6 months.",
		price: 16.60,
		onOffer: true 
	},
	{
		id: "wdc002",
		name: "The Peanutshell Newborn Layette Gift Set for Baby Boys",
		description: "SOFT 100% COTTON CLOTHING: Knit cotton is the top choice of many parents for its cozy, breathable qualities. You’ll love these soft, 100% cotton baby clothes. The sleeping sack makes an excellent sleeper to keep your little one comfortable all night long. You can also use the bodysuits as pajamas or daytime clothes.",
		price: 39.99,
		onOffer: true 
	},
	{
		id: "wdc003",
		name: "Simple Joys by Carter's Unisex Babies' Hooded Sweater Jacket with Sherpa Lining",
		description: "Trusted Carter's quality, every day low prices, and hassle-free tag less packaging-exclusively for Amazon member.",
		price: 17.90,
		onOffer: true 
	},
]


export default product_data